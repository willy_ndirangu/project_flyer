<?php

use App\Http\Flash;
use App\Flyer;

function flash($title = null, $message = null)
{

    $flash = app(Flash::class);

    if (func_num_args() == 0) {
        return $flash;
    }
    return $flash->info($title, $message);
}

function flyer_path(Flyer $flyer){
    return $flyer->zip ."/". str_replace(' ','-',$flyer->street);

}