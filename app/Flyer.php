<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flyer extends Model
{


    protected $fillable = [
        'street',
        'city',
        'zip',
        'country',
        'state',
        'price',
        'description'
    ];

    /**
     * Scope query to those located at a given address
     * @param builder $query
     * @param $zip
     * @param $street
     * @return scope
     */
    public static function locatedAt($zip, $street)
    {

        $street = str_replace('-', ' ', $street);
        return static::where(compact('zip', 'street'))->firstOrFail();
    }

    public function getPriceAttribute($price)
    {
        return '$' . number_format($price);
    }

    public function addPhoto(Photo $photo)
    {
        return $this->photos()->save($photo);
    }

    /**
     * A flyer is composed of many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function photos()
    {

        return $this->hasMany('App\Photo');
    }

    public function owner()
    {

        return $this->belongsTo('App\User', 'user_id');
    }

    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

}
