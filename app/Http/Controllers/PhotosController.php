<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddPhotoRequest;
use App\Flyer;
use App\Photo;
use App\AddPhotoToFlyer;

class PhotosController extends Controller
{
    public function store($zip, $street, AddPhotoRequest $request)
    {

        $flyer = Flyer::locatedAt($zip, $street);
        $photo = $request->file("photo");
        (new AddPhotoToFlyer($flyer, $photo))->save();


    }
    public function destroy($id){
       Photo::findOrFail($id)->delete();

        return back();
    }
}
