@extends('layouts')


@section('content')

    <h1>Selling a home?</h1>

    <div class="row">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <form method="post" action="/flyers" enctype="multipart/form-data">

                @include('flyers.form')
            </form>
    </div>
@stop
