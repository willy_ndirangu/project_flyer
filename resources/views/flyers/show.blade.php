@extends('layouts')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <h1>{!! $flyer->street !!}</h1>

            <h1>{!! $flyer->price !!}</h1>
            <hr>
            <div class="description">{!! nl2br($flyer->description) !!}</div>
        </div>

        <div class="col-md-8 gallery">
            <div class="row">
                @foreach ($flyer->photos->chunk(4) as $set)
                    <div class="row">
                        @foreach ($set as $photo)
                            <form method="POST" action="/photos/{{$photo->id}}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE" id="">
                                <button type="submit">Delete</button>
                            </form>
                            <div class="col-md-3 gallery__image">
                                <a href="/{{$photo->path}}" data-lity>
                                    <img src="/{{ $photo->thumbnail_path }}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>

            @if(Auth::user() && Auth::user()->owns($flyer))
                <form id="addPhotosForm"
                      action="/{{$flyer->zip}}/{{$flyer->street}}/photos"
                      method="POST"
                      class="dropzone">

                    {{csrf_field()}}
                </form>
            @endif
        </div>

    </div>

    </div>


@stop

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFilesize: 5,
            acceptedFiles: '.jpg, .jpeg, .png,.bmp'
        };
    </script>

@stop